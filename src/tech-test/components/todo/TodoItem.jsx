const TodoItem = ({ todo, toggleTodo, deleteTodo, userSession }) => {
  return (
    <>
          <input
            type='checkbox'
            checked={todo.isComplete}
            onChange={() => {}}
            className='mr-2'
          />
          <span>{todo.todolist}</span>
    </>
  )
}

export default TodoItem