'use client';

import { useState, useEffect } from 'react';

import TodoItem from './TodoItem';

const TodoList = () => {

    // intial todo state
    const [todos, setTodos] = useState([]);

    // hook that run once
    useEffect(() => {
        const fetchPosts = async () => {
            // fetches the data from api/todolist using the GET method
            const response = await fetch('api/todolist', {
                method: 'GET'
            });

            // extracts the JSON from the response
            const data = await response.json();
            // sets the todos state with the data
            setTodos(data);
        }

        // triggers the fetchPosts function
        fetchPosts();
    }, [])

    return (
        <>
        {/* render and maps array of each list item */}
        {/* that will pass the data to the TodoItem component */}
            <ul>
                {todos.map((todo) => (
                    <li key={todo._id} className={todo.isComplete ? "line-through" : ""}>
                        <TodoItem
                            todo={todo}
                            toggleTodo={() => { }}
                            deleteTodo={() => { }}
                            userSession={false}
                        />
                    </li>
                ))}
            </ul>

        </>
    )
}

export default TodoList