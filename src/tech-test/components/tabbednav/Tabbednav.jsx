"use client";

import React, { useState } from 'react';
import TodoList from '@components/todo/TodoList';
import TabbedChart from '@components/population/Tabbedchart';

const Tabbednav = () => {
    // initialize the active tab
    const [activeTab, setActiveTab] = useState('tab1');

    // function to updates the active tab
    const handleTabClick = (tab) => {
        setActiveTab(tab);
    };

    return (
        <div className='tabbed_navigation'>
            <section className='feed'>
                <div className='tab_buttons'>
                    {/* when the button is click, the function is called */}
                    <button className='tabbed_btn rounded-l-lg' onClick={() => handleTabClick('tab1')}>To-do</button>
                    <button className='tabbed_btn' onClick={() => handleTabClick('tab2')}>Population</button>
                </div>
                {/* render only when condition is met */}
                {
                    activeTab === 'tab1' &&
                    <TodoList />
                }
                {
                    activeTab === 'tab2' &&
                    <TabbedChart />
                }
            </section>
        </div>
    );
};

export default Tabbednav;