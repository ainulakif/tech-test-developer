// In here, I could implement user authorization through next-auth
// as the no. of user is capped, I have modified the feature to signin from provider into useState hook.

"use client"

import { useState } from "react";
import Link from "next/link";
import Image from "next/image";

const Navbar = () => {
    // initialised loggedIn as false
    const [loggedIn, setLoggedIn] = useState(false);

    // state for dropdown button (mobile navigation)
    const [toggleDropdown, setToggleDropdown] = useState(false);

    // update isLoggedIn when the button is clicked
    const handleLoggedIn = () => {
        setLoggedIn(!loggedIn);
    }

    return (
        <nav className="flex-between w-full mb-8 pt-3">
            <Link href="/" className="flex gap-2 flex-center">
                <Image
                    src="/assets/images/icon-fav.png"
                    alt="Promptopia Logo"
                    width={30}
                    height={30}
                    className="object-contain"
                /> {/* logo */}
                <p className="logo_text">Portfolio</p>
            </Link>

            {/* Desktop Navigation */}
            <div className="sm:flex hidden">
                {/* if user is logged in, render the following */}
                {loggedIn ? (
                    <div className="flex gap-3 md:gap-5">
                        <Link href="/create-post" className="black_btn">
                            Create Post
                        </Link>

                        <button type="button" onClick={handleLoggedIn} className="outline_btn">
                            Sign Out
                        </button>

                        <Link href="/profile">
                            <Image
                                src="/assets/images/icon-fav.png"
                                width={37}
                                height={37}
                                className="rounded-full"
                                alt="profile"
                            />
                        </Link>
                    </div>
                ) : (
                    <button
                        type="button"
                        onClick={handleLoggedIn}
                        className="black_btn"
                    >
                        Sign In
                    </button>

                )}
            </div>

            {/* Mobile Navigation */}
            <div className="sm:hidden flex relative">
                {loggedIn ? (
                    <div className="flex">
                        <Image
                            src={"/assets/images/icon-fav.png"}
                            width={37}
                            height={37}
                            className="rounded-full"
                            alt="profile"
                            // arrow function to toggle the boolean state that will render the Dropdown list
                            onClick={() => setToggleDropdown((prev) => !prev)}
                        />

                        {toggleDropdown && (
                            <div className="dropdown">
                                <Link
                                    href="/profile"
                                    className="dropdown_link"
                                    onClick={() => setToggleDropdown(false)}
                                >
                                    My Profile
                                </Link>
                                <Link
                                    href="/create-post"
                                    className="dropdown_link"
                                    onClick={() => setToggleDropdown(false)}
                                >
                                    Create Post
                                </Link>
                                <button
                                    type="button"
                                    onClick={() => {
                                        setToggleDropdown(false);
                                        handleLoggedIn();
                                    }}
                                    className="mt-5 w-full black_btn"
                                >
                                    Sign Out
                                </button>
                            </div>
                        )}
                    </div>
                ) : (
                    <button
                        type="button"
                        onClick={handleLoggedIn}
                        className="black_btn"
                    >
                        Sign In
                    </button>

                )}
            </div>

        </nav >
    )
}

export default Navbar