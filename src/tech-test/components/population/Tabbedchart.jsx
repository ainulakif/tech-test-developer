"use client";

import { useState, useEffect } from "react";
import ChartDetails from "./ChartDetails";

const TabbedChart = () => {

    // initial result state
    const [result, setResult] = useState([])

    // hook that run once
    useEffect(() => {
        const getCountriesDetails = async () => {
            try {
                // fetches data from the REST Countries API
                const response = await fetch('https://restcountries.com/v3.1/all?fields=name,population')

                // extracts the JSON from the response
                const data = await response.json();

                // sets the result state with the data
                setResult(data);
            } catch (error) {
                console.log(error);
            }
        }

        // triggers the getCountriesDetails function
        getCountriesDetails();
    }, []);

    return (
        // inline style is to properly set the height of the chart
        <section className="w-full flex-center flex-col" style={{ height: "80vh" }}>
            {/* display the data in the ChartDetails component */}
            <ChartDetails output={result} />
        </section>
    )
}

export default TabbedChart