// ChartJS documentation can be read at https://www.chartjs.org/docs/latest/

import {
    Chart as ChartJS,
    CategoryScale,
    LinearScale,
    BarElement,
    Title,
    Tooltip,
    Legend,
} from "chart.js";

// Specifically get the `Bar` Chart
import { Bar } from "react-chartjs-2";
import ChartDataLabels from "chartjs-plugin-datalabels"

// Global Plugins to be applied on all Charts
ChartJS.register(
    CategoryScale,
    LinearScale,
    BarElement,
    Title,
    Tooltip,
    Legend,
    ChartDataLabels
);

// Set custom options to the charts
export const options = {

    maintainAspectRatio: false,
    indexAxis: 'y',
    elements: {
        bar: {
            borderWidth: 2,
        },
    },
    responsive: true,
    scales: {
        // X-axis options
        x: {
            ticks: {
                callback: function (value, index, values) {
                    let number = value;

                    let roundedAndFormatted = number >= 1000000
                        ? Math.floor(number / 1000000) + 'M'
                        : number >= 1000
                            ? Math.floor(number / 1000) + ' thousand'
                            : Math.floor(number).toString();

                    // Return the rounded and formatted value as the x-axis label
                    return roundedAndFormatted;
                }
            }
        }
    },
    plugins: {
        legend: {
            display: false,
            position: 'top',
        },
        title: {
            display: true,
            text: 'Chart.js Population by Country Chart',
        },
        // Y-axis options
        datalabels: {
            align: 'end',
            anchor: 'end',
            color: 'gray',
            formatter: function (value) {
                let number = value;

                let roundedAndFormatted = number >= 1000000
                    ? Math.floor(number / 1000000) + 'M'
                    : number >= 1000
                        ? Math.floor(number / 1000) + 'k'
                        : Math.floor(number).toString();

                return roundedAndFormatted;
            },
        }
    },
};


// output prop is from previous component
const ChartDetails = ({ output }) => {
    return (
        <div className="graph_card">
            {/* 
            - Renders Bar component from the react-chartjs-2 library
            - options' objects are defined above
            - data's objects contain the labels and DATASETS for the chart
            - from the output array, data is sorted by population in descending order, and sliced to include only the top 30 countries 
            */}
            <Bar
                options={options}
                data={{
                    labels: output
                        .sort((a, b) => b.population - a.population)
                        .slice(3, 33)
                        .map(country => country.name.common),
                    datasets: [
                        {
                            label: "Population",
                            data: output
                                .sort((a, b) => b.population - a.population)
                                .slice(3, 33)
                                .map(country => country.population),
                            borderColor: 'rgb(255, 99, 132)',
                            backgroundColor: 'rgba(255, 99, 132, 0.5)',
                        },
                    ],
                    datalabels: {
                        anchor: 'end',
                        align: 'start',
                    }
                }}
            />
        </div>
    )
}

export default ChartDetails;