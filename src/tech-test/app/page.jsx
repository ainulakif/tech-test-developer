// Homepage

import Tabbednav from '@components/tabbednav/Tabbednav';

const Home = () => {
  return (
    <section className="w-full flex-center flex-col">
        <h1 className="head_text head_center">
            {/* underscore means globals.css */}
           Web Application
            <br className="max-md:hidden" />
            <span className="green_gradient text-center"> Simple APIs call</span>
        </h1>
        <p className="desc text-center">
        Web application that interacts with an API.
        </p>
        {/* this component encapsulate the necessary elements and functionality for rendering objects */}
        <Tabbednav />
    </section>
  )
}

export default Home