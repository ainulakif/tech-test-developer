import Navbar from "@components/navbar/Navbar";
import "@styles/globals.css";

export const metadata = {
  title: 'Technical Test Website | Generated by Next.js',
  description: 'This website is generated by Next.js that I use to showcase my current skill sets.',
  keywords: ['Next.js', 'React', 'JavaScript'],
}

const RootLayout = ({ children }) => {
  return (
    <html lang="en">
      <head>
        <link rel="icon" href="/assets/images/icon-fav.png" sizes="32x32" />
      </head>
      <body>
        <div className="main">
          <div className="gradient" />
        </div>

        <main className="app">
          {/* Navbar component is here; so, it only called once for the entire website page */}
          <Navbar />
          {children}
        </main>
      </body>
    </html>
  )
}

export default RootLayout;
