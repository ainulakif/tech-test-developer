# Technical Test Website

## Application's overview

- A [technical test website](https://localhost:3000) that I can use to showcase my current skill sets are when it comes to solving an open-ended problem statement.

- Features
  - To-Do List:
    - Users can ~~create, edit, and delete tasks~~ view in a simple to-do list.
    - The to-do list is stored in a database ~~and can be accessed across sessions~~.
    - ~~Users can mark tasks as done or undone.~~
  - Population Graph:
    - The application fetches population data from REST countries API and visualizes it as an interactive graph.
    - Users can view population trends over different countries.
    - The graph component can be customized using Chart.js library.

- API Routes
  - To-do list: `app/api/todolist`

- Environment Variable
  - MongoDB connection URI string: `MONGODB_URI`

## Application Requirements

- Node.js v20.11.0
- Next.js v14.2.1

## Installation instructions

- Clone this repository:

  - `git clone <link from repo>`
  - `cd tech-test`

- Install dependencies:

  - `npm install`

- Start the development server:

  - `npm run dev`

- Open your browser and navigate to `http://localhost:3000.`

>1. Make sure to add your own MongoDB connection URI string (`\utils\database.js`) to access the [MongoDB database](https://cloud.mongodb.com/).
>2. Add your own database name (`\api\todolist\route.js`)

## Technology stack

Technology stack that are associated with this project:

- [Next.js](https://nextjs.org/): Next.js is a React-based framework that allows server-side rendering and other backend-related tasks.
- [React](https://reactjs.org/): Next.js is built on top of React, so I have be using React components to create user interfaces.
- [Nodejs](https://nodejs.org/en/): It serves as the runtime environment.
- [JavaScript(ES6+)](https://developer.mozilla.org/en-US/docs/Web/JavaScript): I have used JavaScript as my main programming language that follows ES6 syntax.
- [MongoDB](https://www.mongodb.com/): I have used MongoDB as my database.
- [Tailwind CSS](https://tailwindcss.com/): I have used Tailwind CSS as my CSS framework.

### Comments/Notes

- Resources
  - [Next.js Documentation](https://nextjs.org/docs) - Learn about Next.js features and API.
  - [Mongoose Documentation](https://mongoosejs.com/) - Open source library to build a database schema, and connect it to a MongoDB database.
  - [Nextjs with Mongodb](https://github.com/mongodb-developer/nextjs-with-mongodb) - Open source tutorial on how to integrate MongoDB with Next.js.
  - [Chart.js](https://www.chartjs.org/docs/latest/) - Documentation on how to use Chart.js.
  - [REST COUNTRIES 🇵🇪](https://restcountries.com/) - Get information about countries via a RESTful API.
